
/**
 * bài 1
 */
var result = document.getElementById("result");
function timSoNguyenDuongNhoNhat(){
    var i = 0, sum = 0;
    while(sum < 10000){
        i++;
        sum = sum + i;
    }
    result.innerHTML = `<h3>Số nguyên dương nhỏ nhất: ${i}</h3>`;
}
/**
 * bài 2
 */
function tinhTong(){
    var x =  document.getElementById("soX").value*1;
    var n = document.getElementById("soN").value*1;
    var sum = 0;
    for(var i = 1; i <= n; i++) {
        sum = sum + (Math.pow(x,i));
    }
    result.innerHTML = `<h3>Tổng: ${sum}</h3>`;
}

/**
 * bài 3
 */
function tinhGiaiThua() {
    var n = document.getElementById("n-bai3").value*1;
    var sum = 1;
    for(var i = 1; i <= n ; i++){
        sum = sum*i;
    }
    result.innerHTML = `<h3>Giai thừa: ${sum}</h3>`;
}

/**
 * bài 4
 */
//bai4
function taoTheDiv(){
    var content = [];
    for(var i = 1; i <= 10; i++){
        if(i %2 == 0){
            content[i-1] = `<div style="background-color: red;">Div chẵn ${i}</div>`;
        }
        else {
            content[i-1] = `<div style="background-color: blue;">Div lẻ ${i}</div>`;
        }
    }
    arr = [1,3,4,5,6,7,4,3,2]
    arr.join("|")
    console.log("🚀 ~ file: index.js:54 ~ arr", arr)
    console.log("🚀 ~ file: index.js:54 ~ taoTheDiv ~ content", content)
    result.innerHTML = `${content.join('')}`
}